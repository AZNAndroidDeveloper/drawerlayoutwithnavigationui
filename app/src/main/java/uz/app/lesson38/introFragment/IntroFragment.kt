package uz.app.lesson38.introFragment

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.google.android.material.navigation.NavigationView
import uz.app.lesson38.HomeFragment
import uz.app.lesson38.R
import uz.app.lesson38.SettingFragment
import uz.app.lesson38.databinding.FragmentIntroBinding

class IntroFragment : Fragment(R.layout.fragment_intro),NavigationView.OnNavigationItemSelectedListener {
    private lateinit var binding:FragmentIntroBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        val toggle: ActionBarDrawerToggle = ActionBarDrawerToggle(requireActivity(), binding.drawerLayout, binding.toolbar,
            R.string.open, R.string.close
        )

        with(binding) {
            drawerLayout.addDrawerListener(toggle)
            navigationView.setNavigationItemSelectedListener(this@IntroFragment)

        }
        toggle.syncState()

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.home->{fragmentManager!!.beginTransaction().replace(binding.fragmentContainer.id,HomeFragment()).commit()}
            R.id.setting->{fragmentManager!!.beginTransaction().replace(binding.fragmentContainer.id,SettingFragment()).commit()}
            R.id.log_out-> requireActivity().finish()

        }
        with(binding){
            drawerLayout.closeDrawer(GravityCompat.START)
        }
        return true

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_item,menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_text ->requireActivity().finish()
        }

        return super.onOptionsItemSelected(item)
    }

}