package uz.app.lesson38

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import uz.app.lesson38.databinding.ActivityMainBinding
import uz.app.lesson38.introFragment.IntroFragment

class MainActivity : AppCompatActivity() {
    private  val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        supportFragmentManager.beginTransaction().replace(binding.frameLayout.id,IntroFragment()).commit()

    }
}